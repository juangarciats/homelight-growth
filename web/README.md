# Web

Application to list public gists from Github. Built using React + Parcel.

## Getting started

1. Installing dependencies

```bash
npm i
```

2. Run project

```bash
npm run dev
```

3. Access project on `http://localhost:3000`

⚠️ _Please note that you must also run the API to fetch actual data_ ⚠️

## Running tests

In order to run the test suite, please execute:

```bash
npm run test
```

To collect coverage, you should run:

```bash
npm run test:coverage
```
