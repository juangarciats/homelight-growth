import { fireEvent } from '@testing-library/react';
import { renderHook } from '@testing-library/react-hooks';
import { useScrollObserver } from '../../../src/hooks/useScrollObserver';

describe('useScrollObserver', () => {
  describe('when user is not at the bottom of the page', () => {
    it('should return hasReachedEnd as false', () => {
      const { result } = renderHook(() => useScrollObserver());
      expect(result.current.hasReachedEnd).toBeFalsy();
    });
  });

  describe('when user is at the bottom of the page', () => {
    it('should return hasReachedEnd as false', async () => {
      const { result } = renderHook(() => useScrollObserver());
      fireEvent.scroll(window, { target: { scrollY: Infinity } });
      expect(result.current.hasReachedEnd).toBeTruthy();
    });
  });
});
