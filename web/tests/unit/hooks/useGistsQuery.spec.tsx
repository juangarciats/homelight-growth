import { renderHook } from '@testing-library/react-hooks';
import { useInfiniteQuery } from 'react-query';
import { useGistsQuery } from '../../../src/hooks/useGistsQuery';
import { useScrollObserver } from '../../../src/hooks/useScrollObserver';
import { useGistStore } from '../../../src/store/gistStore';
import { filterGistsByDescription, sortGistsByDate } from '../../../src/utils/gistUtils';
import { GistFactory } from '../../factories/gistFactory';

jest.mock('react-query', () => ({
  useInfiniteQuery: jest.fn(),
}));

jest.mock('../../../src/hooks/useScrollObserver', () => ({
  useScrollObserver: jest.fn(),
}));

jest.mock('../../../src/store/gistStore', () => ({
  useGistStore: jest.fn(),
}));

jest.mock('../../../src/utils/gistUtils', () => ({
  filterGistsByDescription: jest.fn(),
  sortGistsByDate: jest.fn(),
}));

describe('useGistsQuery', () => {
  const expectedGists = GistFactory.buildList(3);
  const data = { pages: [{ page: 1, per_page: 10, items: expectedGists }] };
  const isLoading = false;
  const fetchNextPageMock = jest.fn();

  beforeEach(() => {
    (useGistStore as unknown as jest.Mock).mockReturnValue({ searchBy: '', sortBy: 'ASC' });
    (sortGistsByDate as jest.Mock).mockReturnValue(expectedGists);
    (filterGistsByDescription as jest.Mock).mockReturnValue(expectedGists);
    (useScrollObserver as jest.Mock).mockReturnValue({ hasReachedEnd: false });
    (useInfiniteQuery as jest.Mock).mockReturnValue({
      data,
      isLoading,
      fetchNextPage: fetchNextPageMock,
    });
  });

  it('should return list of gists', () => {
    const { result } = renderHook(() => useGistsQuery());
    expect(result.current.gists).toBe(expectedGists);
  });

  it('should return boolean indicating if query is loading', () => {
    const { result } = renderHook(() => useGistsQuery());
    expect(result.current.isLoading).toBe(isLoading);
  });

  it('should sort list', () => {
    renderHook(() => useGistsQuery());
    expect(sortGistsByDate).toHaveBeenCalledWith(expectedGists, 'ASC');
  });

  describe('when at the bottom of list', () => {
    beforeEach(() => {
      (useScrollObserver as jest.Mock).mockReturnValue({ hasReachedEnd: true });
    });

    it('should fetch more items', () => {
      renderHook(() => useGistsQuery());
      expect(fetchNextPageMock).toHaveBeenCalled();
    });
  });

  describe('when there is a filter applied', () => {
    beforeEach(() => {
      (useGistStore as unknown as jest.Mock).mockReturnValue({ searchBy: 'query', sortBy: 'ASC' });
    });

    it('should filter list by query', () => {
      renderHook(() => useGistsQuery());
      expect(filterGistsByDescription).toHaveBeenCalledWith(expectedGists, 'query');
    });
  });
});
