import toast from 'react-hot-toast';
import { showToast, toastStyle } from '../../../src/utils/toastUtils';

jest.mock('react-hot-toast');

describe('showToast', () => {
  it('should call library to show toast with given message', () => {
    showToast('error', 'Something went wrong', '⛔️');
    expect(toast.error).toHaveBeenCalledWith('Something went wrong', {
      icon: '⛔️',
      style: toastStyle,
    });
  });
});
