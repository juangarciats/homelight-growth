import { GistFactory } from '../../factories/gistFactory';
import { filterGistsByDescription, sortGistsByDate } from '../../../src/utils/gistUtils';

describe('filterGistsByDescription', () => {
  const gists = [
    GistFactory.build({ description: undefined }),
    GistFactory.build({ description: 'How to code in Python' }),
    GistFactory.build({ description: 'JAVA CODE' }),
    GistFactory.build({ description: 'Random description' }),
  ];

  it('should filter gists that includes given query without considering letter case', () => {
    const result = filterGistsByDescription(gists, 'code');
    expect(result).toHaveLength(2);
    expect(result[0].description).toBe('How to code in Python');
    expect(result[1].description).toBe('JAVA CODE');
  });
});

describe('sortGistsByDate', () => {
  const gists = [
    GistFactory.build({ created_at: '2022-01-04' }),
    GistFactory.build({ created_at: '2022-01-02' }),
    GistFactory.build({ created_at: '2022-01-01' }),
    GistFactory.build({ created_at: '2022-01-03' }),
  ];

  describe('when order is ASC', () => {
    const order = 'ASC';

    it('should sort gists from oldest to newest', () => {
      const result = sortGistsByDate(gists, order);
      expect(result[0].created_at).toBe('2022-01-01');
      expect(result[1].created_at).toBe('2022-01-02');
      expect(result[2].created_at).toBe('2022-01-03');
      expect(result[3].created_at).toBe('2022-01-04');
    });
  });

  describe('when order is DESC', () => {
    const order = 'DESC';

    it('should sort gists from newest to oldest', () => {
      const result = sortGistsByDate(gists, order);
      expect(result[0].created_at).toBe('2022-01-04');
      expect(result[1].created_at).toBe('2022-01-03');
      expect(result[2].created_at).toBe('2022-01-02');
      expect(result[3].created_at).toBe('2022-01-01');
    });
  });
});
