import axios from 'axios';
import { addStarToGist, fetchGists, removeStarFromGist } from '../../../src/api/requests';
import { Gist } from '../../../src/types/Gist';
import { PaginatedResponse } from '../../../src/types/PaginatedResponse';
import { GistFactory } from '../../factories/gistFactory';

jest.mock('axios', () => ({
  get: jest.fn(),
  put: jest.fn(),
  delete: jest.fn(),
}));

describe('fetchGists', () => {
  let response: PaginatedResponse<Gist>;
  const expectedGists = GistFactory.buildList(3);

  beforeEach(async () => {
    (axios.get as jest.Mock).mockResolvedValue({ data: { items: expectedGists } });
    response = await fetchGists({});
  });

  it('should call api with proper url and params', () => {
    expect(axios.get).toHaveBeenCalledWith('http://localhost:8080/api/gists', {
      params: { page: 1 },
    });
  });

  it('should return a paginated list of gists', () => {
    expect(response.items).toEqual(expectedGists);
  });
});

describe('addStarToGist', () => {
  const id = '123';

  beforeEach(async () => {
    await addStarToGist(id);
  });

  it('should call api with proper url', () => {
    expect(axios.put).toHaveBeenCalledWith('http://localhost:8080/api/gists/123/star');
  });
});

describe('removeStarFromGist', () => {
  const id = '123';

  beforeEach(async () => {
    await removeStarFromGist(id);
  });

  it('should call api with proper url', () => {
    expect(axios.delete).toHaveBeenCalledWith('http://localhost:8080/api/gists/123/star');
  });
});
