import React from 'react';
import { render, screen } from '@testing-library/react';
import { RiChat1Fill } from 'react-icons/ri';
import { GistStats } from '../../../../src/components/gists/GistStats';

describe('<GistStats />', () => {
  beforeEach(() => {
    render(<GistStats icon={RiChat1Fill} value={123} />);
  });

  it('should render icon', () => {
    expect(screen.getByTestId('gist-stats-icon')).toBeVisible();
  });

  it('should render value', () => {
    expect(screen.getByText('123')).toBeVisible();
  });
});
