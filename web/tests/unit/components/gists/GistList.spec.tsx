import React from 'react';
import { render, screen } from '@testing-library/react';
import { GistList } from '../../../../src/components/gists/GistList';
import { useGistsQuery } from '../../../../src/hooks/useGistsQuery';
import { Gist } from '../../../../src/types/Gist';
import { GistFactory } from '../../../factories/gistFactory';

jest.mock('../../../../src/hooks/useGistsQuery', () => ({
  useGistsQuery: jest.fn(),
}));

describe('<GistList />', () => {
  const setup = (gists: Gist[], isLoading?: boolean) => {
    (useGistsQuery as jest.Mock).mockReturnValue({ gists, isLoading });
    render(<GistList />);
  };

  describe('when component is loading', () => {
    const isLoading = true;

    beforeEach(() => {
      setup([], isLoading);
    });

    it('should render a list of skeleton gist cards', () => {
      expect(screen.getAllByTestId('skeleton-gist-card')).toHaveLength(10);
    });
  });

  describe('when component is not loading', () => {
    const isLoading = false;

    describe('and no gists were found', () => {
      const gists: Gist[] = [];

      beforeEach(() => {
        setup(gists, isLoading);
      });

      it('should render a warning message', () => {
        expect(screen.getByText('No gist found. Please try another filter')).toBeVisible();
      });
    });

    describe('and gists were properly found', () => {
      const gists = GistFactory.buildList(6);

      beforeEach(() => {
        setup(gists, isLoading);
      });

      it('should render a card for each gist', () => {
        expect(screen.getAllByText('Open in Github')).toHaveLength(gists.length);
      });
    });
  });
});
