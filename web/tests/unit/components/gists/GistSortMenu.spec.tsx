import React from 'react';
import { render, screen } from '@testing-library/react';
import { GistSortMenu } from '../../../../src/components/gists/GistSortMenu';
import { useGistStore } from '../../../../src/store/gistStore';
import userEvent from '@testing-library/user-event';

jest.mock('../../../../src/store/gistStore', () => ({
  useGistStore: jest.fn(),
}));

describe('<GistSortMenu />', () => {
  const setSortByMock = jest.fn();

  beforeEach(() => {
    (useGistStore as unknown as jest.Mock).mockReturnValue({
      sortBy: 'ASC',
      setSortBy: setSortByMock,
    });

    render(<GistSortMenu />);
  });

  it('should render button with current order', () => {
    const button = screen.getByRole('button');
    expect(button.textContent).toMatch(/Sort by Oldest/);
  });

  describe('when button is clicked', () => {
    beforeEach(() => {
      const button = screen.getByRole('button');
      userEvent.click(button);
    });

    it('should open menu with order options', () => {
      const options = screen.getAllByRole('menuitem');
      expect(options.some((option) => option.textContent === 'Newest')).toBeTruthy();
      expect(options.some((option) => option.textContent === 'Oldest')).toBeTruthy();
    });

    describe('and an option is selected', () => {
      beforeEach(() => {
        const option = screen.getByText('Newest');
        userEvent.click(option);
      });

      it('should update store', () => {
        expect(setSortByMock).toHaveBeenCalledWith('DESC');
      });
    });
  });
});
