import React from 'react';
import { render, screen } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import { GistSearch } from '../../../../src/components/gists/GistSearch';
import { useGistStore } from '../../../../src/store/gistStore';

jest.mock('../../../../src/store/gistStore', () => ({
  useGistStore: jest.fn(),
}));

describe('<GistSearch />', () => {
  const setSearchByMock = jest.fn();

  beforeEach(() => {
    (useGistStore as unknown as jest.Mock).mockReturnValue({
      searchBy: '',
      setSearchBy: setSearchByMock,
    });
    render(<GistSearch />);
  });

  it('should render input', () => {
    const input = screen.getByPlaceholderText('Filter by description');
    expect(input).toBeVisible();
  });

  describe('when value changes', () => {
    beforeEach(() => {
      const input = screen.getByRole('textbox');
      userEvent.type(input, 'A');
    });

    it('should update store', () => {
      expect(setSearchByMock).toHaveBeenCalledWith('A');
    });
  });
});
