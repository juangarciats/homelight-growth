import { render, screen } from '@testing-library/react';
import React from 'react';
import { GistCard } from '../../../../src/components/gists/GistCard';
import { GistFactory } from '../../../factories/gistFactory';

describe('<GistCard />', () => {
  const gist = GistFactory.build({
    description: 'My gist',
    created_by: 'juangarcia',
    created_at: '2022-03-17T17:38:00.000Z',
    files_count: 123,
    comments_count: 321,
    star_count: 999,
  });

  beforeEach(() => {
    render(<GistCard gist={gist} />);
  });

  it('should render description', () => {
    expect(screen.getByText('My gist')).toBeTruthy();
  });

  it('should render creator username', () => {
    expect(screen.getByText(/juangarcia/)).toBeTruthy();
  });

  it('should render formatted creation date', () => {
    expect(screen.getByText(/2022-03-17 02:38 PM/)).toBeTruthy();
  });

  it('should render number of files', () => {
    expect(screen.getByText(gist.files_count)).toBeTruthy();
  });

  it('should render number of comments', () => {
    expect(screen.getByText(gist.comments_count)).toBeTruthy();
  });

  it('should render number of stars', () => {
    expect(screen.getByText(gist.star_count)).toBeTruthy();
  });

  it('should render button to star a gist', () => {
    expect(screen.getByText('Add a star')).toBeTruthy();
  });

  it('should render link to github', () => {
    const link = screen.getByRole('link', { name: /Open in Github/ });
    expect(link.getAttribute('href')).toBe(gist.url);
  });

  describe('when description is empty', () => {
    beforeEach(() => {
      render(<GistCard gist={{ ...gist, description: undefined }} />);
    });

    it('should render description as "Unnamed"', () => {
      expect(screen.getByText('Unnamed')).toBeTruthy();
    });
  });
});
