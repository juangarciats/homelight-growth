import { render, screen } from '@testing-library/react';
import { GistStarButton } from '../../../../src/components/gists/GistStarButton';
import React from 'react';
import userEvent from '@testing-library/user-event';
import { addStarToGist, removeStarFromGist } from '../../../../src/api/requests';
import { showToast } from '../../../../src/utils/toastUtils';

jest.mock('../../../../src/api/requests', () => ({
  addStarToGist: jest.fn(),
  removeStarFromGist: jest.fn(),
}));

jest.mock('../../../../src/utils/toastUtils', () => ({
  showToast: jest.fn(),
}));

describe('<GistStarButton />', () => {
  const id = '123';
  const onChangeMock = jest.fn();

  beforeEach(() => {
    render(<GistStarButton id={id} onChange={onChangeMock} />);
  });

  describe('when button is clicked', () => {
    beforeEach(async () => {
      const addButton = screen.getByText('Add a star');
      userEvent.click(addButton);
    });

    it('should call api to add star', () => {
      expect(addStarToGist).toHaveBeenCalledWith(id);
    });

    it('should show toast', () => {
      expect(showToast).toHaveBeenCalledWith('success', 'Gist successfully starred', '🤩');
    });

    it('should call onChange with new value', () => {
      expect(onChangeMock).toHaveBeenCalledWith(true);
    });

    it('should change button text to removal version', async () => {
      const addButton = await screen.findByText('Remove star');
      expect(addButton).toBeInTheDocument();
    });

    describe('and button is clicked again', () => {
      beforeEach(async () => {
        const removeButton = await screen.findByText('Remove star');
        userEvent.click(removeButton);
      });

      it('should call api to remove star', () => {
        expect(removeStarFromGist).toHaveBeenCalledWith(id);
      });

      it('should show toast', () => {
        expect(showToast).toHaveBeenCalledWith('success', 'Gist star has been removed', '😕');
      });

      it('should call onChange with new value', () => {
        expect(onChangeMock).toHaveBeenCalledWith(false);
      });

      it('should change button text to previous version', () => {
        expect(screen.getByText('Add a star')).toBeVisible();
      });
    });
  });
});
