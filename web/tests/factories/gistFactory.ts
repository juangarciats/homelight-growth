import faker from 'faker';
import { Factory } from 'rosie';
import { Gist } from '../../src/types/Gist';

export const GistFactory = Factory.define<Gist>('Gist')
  .attr('id', faker.datatype.uuid)
  .attr('url', faker.internet.url)
  .attr('description', faker.random.words)
  .attr('comments_count', faker.datatype.number)
  .attr('files_count', faker.datatype.number)
  .attr('star_count', faker.datatype.number)
  .attr('created_at', () => faker.date.past().toUTCString())
  .attr('created_by', faker.internet.userName);
