import { Gist } from '../types/Gist';
import { isBefore } from 'date-fns';

export const filterGistsByDescription = (gists: Gist[], query: string): Gist[] => {
  return gists.filter((item) => item.description?.toLowerCase().includes(query.toLowerCase()));
};

export const sortGistsByDate = (gists: Gist[], order: 'ASC' | 'DESC') => {
  return gists.sort((a, b) => {
    const isDateBefore = isBefore(new Date(a.created_at), new Date(b.created_at));

    if (order === 'ASC') {
      return isDateBefore ? -1 : 1;
    }

    return isDateBefore ? 1 : -1;
  });
};
