import toast from 'react-hot-toast';

type ToastType = 'success' | 'error';

export const toastStyle = {
  background: '#1d1e26',
  color: '#fff',
};

export const showToast = (type: ToastType, message: string, icon?: string): void => {
  toast[type]?.(message, { icon, style: toastStyle });
};
