export type PaginatedResponse<T> = {
  page: number;
  per_page: number;
  items: T[];
};
