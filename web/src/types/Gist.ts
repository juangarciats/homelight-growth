export type Gist = {
  id: string;
  url: string;
  description?: string;
  comments_count: number;
  files_count: number;
  star_count: number;
  created_at: string;
  created_by: string;
};
