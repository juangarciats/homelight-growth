import React from 'react';
import { Layout } from '../components/shared/Layout';
import { GistList } from '../components/gists/GistList';
import { GistSearch } from '../components/gists/GistSearch';
import { GistSortMenu } from '../components/gists/GistSortMenu';

export const GistsPage = (): JSX.Element => {
  return (
    <Layout>
      <div className="flex flex-col  md:flex-row md:justify-between md:items-center">
        <GistSearch />
        <GistSortMenu />
      </div>
      <div className="mt-3 md:mt-6">
        <GistList />
      </div>
    </Layout>
  );
};
