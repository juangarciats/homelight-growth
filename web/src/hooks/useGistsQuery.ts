import { useEffect, useMemo } from 'react';
import { useInfiniteQuery } from 'react-query';
import { fetchGists } from '../api/requests';
import { useGistStore } from '../store/gistStore';
import { Gist } from '../types/Gist';
import { filterGistsByDescription, sortGistsByDate } from '../utils/gistUtils';
import { useScrollObserver } from './useScrollObserver';

export const useGistsQuery = (): {
  gists: Gist[];
  isLoading: boolean;
} => {
  const { searchBy, sortBy } = useGistStore();
  const { hasReachedEnd } = useScrollObserver();
  const { data, isLoading, fetchNextPage } = useInfiniteQuery('gists', fetchGists, {
    getNextPageParam: ({ page }) => page + 1,
  });

  const gists: Gist[] = useMemo(() => {
    const items = data?.pages.flatMap((page) => page.items) ?? [];
    const filteredGists = filterGistsByDescription(items, searchBy);
    return sortGistsByDate(filteredGists, sortBy);
  }, [data, searchBy, sortBy]);

  useEffect(() => {
    const shouldFetchMore = gists.length > 0 && !searchBy && hasReachedEnd;

    if (shouldFetchMore) {
      fetchNextPage();
    }
  }, [gists, searchBy, hasReachedEnd, fetchNextPage]);

  return { gists, isLoading };
};
