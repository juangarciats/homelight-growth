import { useEffect, useState } from 'react';

export const useScrollObserver = (): { hasReachedEnd: boolean } => {
  const [hasReachedEnd, setHasReachedEnd] = useState(false);

  useEffect(() => {
    const scrollListener = () => {
      if (window.innerHeight + window.scrollY >= document.body.offsetHeight) {
        setHasReachedEnd(true);
      } else {
        setHasReachedEnd(false);
      }
    };

    window.addEventListener('scroll', scrollListener);

    return () => {
      window.removeEventListener('scroll', scrollListener);
    };
  }, []);

  return { hasReachedEnd };
};
