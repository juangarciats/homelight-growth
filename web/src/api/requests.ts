import axios from 'axios';
import { Gist } from '../types/Gist';
import { PaginatedResponse } from '../types/PaginatedResponse';

const BASE_URL = 'http://localhost:8080/api';

export const fetchGists = async ({ pageParam = 1 }): Promise<PaginatedResponse<Gist>> => {
  const { data } = await axios.get(`${BASE_URL}/gists`, {
    params: { page: pageParam },
  });

  return data;
};

export const addStarToGist = async (id: string): Promise<void> => {
  await axios.put(`${BASE_URL}/gists/${id}/star`);
};

export const removeStarFromGist = async (id: string): Promise<void> => {
  await axios.delete(`${BASE_URL}/gists/${id}/star`);
};
