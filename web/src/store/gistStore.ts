import create from 'zustand';
import produce from 'immer';

type GistSortOrder = 'ASC' | 'DESC';

type GistStore = {
  searchBy: string;
  sortBy: GistSortOrder;
  setSearchBy: (_query: string) => void;
  setSortBy: (_order: GistSortOrder) => void;
};

export const useGistStore = create<GistStore>((set) => ({
  searchBy: '',
  sortBy: 'DESC',

  setSearchBy: (query: string) =>
    set(
      produce((state: GistStore) => {
        state.searchBy = query;
      }),
    ),

  setSortBy: (order: GistSortOrder) =>
    set(
      produce((state: GistStore) => {
        state.sortBy = order;
      }),
    ),
}));
