import React from 'react';

export type LayoutProps = {
  children: JSX.Element | JSX.Element[];
};

export const Layout = ({ children }: LayoutProps): JSX.Element => {
  return (
    <div className="min-h-screen bg-[#161820] text-white">
      <div className="py-6 container mx-auto px-8">
        <header>
          <h1 className="font-bold text-3xl text-[#9069FE]">gists.</h1>
        </header>
        <main className="mt-6">{children}</main>
      </div>
    </div>
  );
};
