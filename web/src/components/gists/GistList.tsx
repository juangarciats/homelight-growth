import React from 'react';
import { useGistsQuery } from '../../hooks/useGistsQuery';
import { GistCard } from './GistCard';
import { SkeletonGistCard } from './SkeletonGistCard';

export const GistList = (): JSX.Element | null => {
  const { gists, isLoading } = useGistsQuery();

  if (isLoading) {
    return (
      <>
        {[...Array(10)].map((_, index) => (
          <SkeletonGistCard key={index} />
        ))}
      </>
    );
  }

  if (!gists.length) {
    return <p>No gist found. Please try another filter</p>;
  }

  return (
    <>
      {gists.map((gist) => (
        <GistCard key={gist.id} gist={gist} />
      ))}
    </>
  );
};
