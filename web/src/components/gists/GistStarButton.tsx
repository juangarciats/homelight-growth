import clsx from 'clsx';
import React, { useState } from 'react';
import { RiStarFill } from 'react-icons/ri';
import { addStarToGist, removeStarFromGist } from '../../api/requests';
import { showToast } from '../../utils/toastUtils';

type GistStarButtonProps = {
  id: string;
  onChange: (_isStarred: boolean) => void;
};

export const GistStarButton = ({ id, onChange }: GistStarButtonProps) => {
  const [isStarred, setIsStarred] = useState(false);

  const toggleStar = async () => {
    const nextValue = !isStarred;

    try {
      if (nextValue) {
        await addStarToGist(id);
        showToast('success', 'Gist successfully starred', '🤩');
      } else {
        await removeStarFromGist(id);
        showToast('success', 'Gist star has been removed', '😕');
      }

      setIsStarred(nextValue);
      onChange(nextValue);
    } catch (err) {
      showToast('error', 'Something went wrong');
    }
  };

  return (
    <div
      role="button"
      tabIndex={0}
      onKeyDown={toggleStar}
      onClick={toggleStar}
      className={clsx('flex flex-row items-center mt-1 hover:underline hover:cursor-pointer', {
        'decoration-[#9069FE]': isStarred,
      })}
    >
      <RiStarFill
        className={clsx('mr-2', {
          'text-[#9069FE]': isStarred,
        })}
      />

      {isStarred ? (
        <span className="text-sm text-[#9069FE]">Remove star</span>
      ) : (
        <span className="text-sm">Add a star</span>
      )}
    </div>
  );
};
