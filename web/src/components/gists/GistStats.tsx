/* eslint-disable import/named */
import React from 'react';
import { IconType } from 'react-icons';

type GistStatsProps = {
  icon: IconType;
  value: string | number;
};

export const GistStats = ({ icon: Icon, value }: GistStatsProps): JSX.Element => {
  return (
    <div className="flex flex-row items-center justify-end pr-3">
      <Icon className="mr-3 text-[#9069FE]" data-testid="gist-stats-icon" /> {value}
    </div>
  );
};
