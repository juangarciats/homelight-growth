import React from 'react';
import { useGistStore } from '../../store/gistStore';

export const GistSearch = () => {
  const { searchBy, setSearchBy } = useGistStore();

  return (
    <input
      placeholder="Filter by description"
      value={searchBy ?? ''}
      onChange={(e) => setSearchBy(e.target.value)}
      className="bg-transparent rounded-md border-2 border-[#35374A] hover:border-[#9069FE] focus:border-[#9069FE] px-3 py-1 outline-none min-w-[30vw]"
    />
  );
};
