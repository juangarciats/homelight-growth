import 'react-loading-skeleton/dist/skeleton.css';
import React from 'react';
import Skeleton, { SkeletonTheme } from 'react-loading-skeleton';

export const SkeletonGistCard = (): JSX.Element => {
  return (
    <SkeletonTheme baseColor="#332754" highlightColor="#9069FE">
      <div className="bg-[#35374A] p-3 rounded-xl my-3" data-testid="skeleton-gist-card">
        <Skeleton width="50%" />
        <Skeleton width="25%" />
        <Skeleton width="15%" className="mt-3" />
        <Skeleton count={2} width="12.5%" className="mt-2" />
      </div>
    </SkeletonTheme>
  );
};
