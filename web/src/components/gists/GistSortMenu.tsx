import '@szhsin/react-menu/dist/index.css';
import '@szhsin/react-menu/dist/transitions/slide.css';
import React from 'react';
import { Menu, MenuItem } from '@szhsin/react-menu';
import { RiArrowDropDownFill } from 'react-icons/ri';
import { useGistStore } from '../../store/gistStore';

export const GistSortMenu = () => {
  const { sortBy, setSortBy } = useGistStore();
  const sortOrderMap: Record<string, string> = {
    ASC: 'Oldest',
    DESC: 'Newest',
  };

  return (
    <div className="flex flex-row items-center mt-2 md:mt-0 justify-end">
      <Menu
        transition
        menuButton={
          <div role="button" tabIndex={0} className="cursor-pointer">
            <p className="flex flex-row items-center">
              Sort by
              <span className="font-bold text-[#9069FE] whitespace-pre">
                {` ${sortOrderMap[sortBy]}`}
              </span>
              <RiArrowDropDownFill size={30} />
            </p>
          </div>
        }
      >
        {Object.keys(sortOrderMap).map((order: string) => (
          <MenuItem
            key={order}
            onClick={() => setSortBy(order as 'ASC' | 'DESC')}
            className="text-sm"
          >
            {sortOrderMap[order]}
          </MenuItem>
        ))}
      </Menu>
    </div>
  );
};
