import clsx from 'clsx';
import React, { useState } from 'react';
import { Gist } from '../../types/Gist';
import { format } from 'date-fns';
import { RiGithubFill, RiChat1Fill, RiFile2Fill, RiStarFill } from 'react-icons/ri';
import { GistStarButton } from './GistStarButton';
import { GistStats } from './GistStats';

export type GistCardProps = {
  gist: Gist;
};

export const GistCard = ({ gist }: GistCardProps): JSX.Element => {
  const [starCount, setStarCount] = useState(gist.star_count);

  return (
    <div className="bg-[#35374A] p-3 rounded-xl my-3">
      <p
        className={clsx('text-lg font-extrabold break-words', {
          'text-gray-400': !gist.description,
        })}
      >
        {gist.description || 'Unnamed'}
      </p>

      <p className="text-xs text-gray-300">
        Created by
        <span className="font-bold text-[#9069FE]">{` ${gist.created_by} `}</span>
        at
        <span className="font-bold text-[#9069FE]">
          {` ${format(new Date(gist.created_at), 'yyyy-MM-dd hh:mm a')}`}
        </span>
      </p>

      <div className="flex flex-row items-center justify-start mt-3">
        <GistStats icon={RiFile2Fill} value={gist.files_count} />
        <GistStats icon={RiChat1Fill} value={gist.comments_count} />
        <GistStats icon={RiStarFill} value={starCount} />
      </div>

      <a
        href={gist.url}
        target="_blank"
        rel="noreferrer"
        className="flex flex-row items-center mt-3 hover:underline"
      >
        <RiGithubFill className="mr-2" />
        <span className="text-sm">Open in Github</span>
      </a>

      <GistStarButton
        id={gist.id}
        onChange={(isStarred) => setStarCount(isStarred ? starCount + 1 : starCount - 1)}
      />
    </div>
  );
};
