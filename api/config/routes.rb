Rails.application.routes.draw do
  get "/api/gists", to: "gists#index"
  put "/api/gists/:id/star", to: "gists#add_star"
  delete "/api/gists/:id/star", to: "gists#delete_star"
end
