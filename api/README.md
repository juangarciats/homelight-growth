# API

API to fetch public gists from Github. Built using Ruby on Rails.

## Getting started

1. Installing dependencies

```bash
bundle
```

2. Execute migrations

```bash
rails db:migrate
```

3. Launch web server on proper port

```bash
rails server -p 8080
```

4. Interact with application on `http://localhost:8080`

## Running tests

In order to run the test suite, please execute:

```bash
bundle exec rspec
```
