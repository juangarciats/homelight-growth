class GistsController < ActionController::API
  def index
    gists_response = GithubApiGateway.get_public_gists(params[:page], params[:per_page])
    external_ids = gists_response[:items].map { |item| item['id'] }
    existing_gists = Gist.where(external_id: external_ids).all
    gists_response[:items] = get_gists_star_count(gists_response[:items], existing_gists)
    render status: 200, json: gists_response
  end

  def add_star
    gist = Gist.find_or_create(params[:id])
    gist.add_star
    render status: 201, json: gist
  end

  def delete_star
    gist = Gist.find_by(external_id: params[:id])

    return render status: 404, json: { message: 'Gist not found' } unless gist

    gist.remove_star
    render status: 200, json: gist
  end

  private

  def get_gists_star_count(items, existing_gists)
    gists = []

    items.each do |item|
      existing_gist = existing_gists.detect { |g| g[:external_id] == item['id'] }
      star_count = existing_gist.nil? ? nil : existing_gist[:star_count]
      gists << build_gist_from_json(item, star_count)
    end

    gists
  end

  def build_gist_from_json(json, star_count)
    star_count ||= 0

    {
      id: json['id'],
      url: json['html_url'],
      description: json['description'],
      comments_count: json['comments'],
      files_count: json['files'].count,
      star_count:,
      created_at: json['created_at'],
      created_by: json['owner']['login']
    }
  end
end
