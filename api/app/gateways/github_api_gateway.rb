require 'faraday'
require 'faraday/net_http'

Faraday.default_adapter = :net_http

class GithubApiGateway
  @base_url = 'https://api.github.com'

  def self.get_public_gists(page = nil, per_page = nil)
    page = page ? Integer(page) : 1
    per_page = per_page ? Integer(per_page) : 10
    response = Faraday.get("#{@base_url}/gists/public", { page:, per_page: })
    build_paginated_response(response, page, per_page)
  end

  def self.build_paginated_response(response, page, per_page)
    {
      page:,
      per_page:,
      items: JSON.parse(response.body)
    }
  end
end
