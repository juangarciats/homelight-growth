class GistNotFoundError < StandardError; end

class Gist < ApplicationRecord
  validates :external_id, presence: true

  def self.find_or_create(external_id)
    gist = Gist.find_by(external_id:)

    return gist unless gist.nil?

    gist = Gist.new(external_id:)
    gist.save
    gist
  end

  def add_star
    increment(:star_count)
    save
  end

  def remove_star
    decrement(:star_count)
    save
  end
end
