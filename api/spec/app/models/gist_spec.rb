describe Gist do
  it 'external_id should be present' do
    gist = Gist.new
    gist.save
    expect(gist).to_not be_valid
  end

  describe '.find_or_create' do
    existing_gist = Gist.new(external_id: '123', star_count: 50)

    before do
      existing_gist.save
    end

    context 'when gist exists' do
      it 'should return existing gist' do
        gist = Gist.find_or_create('123')
        expect(gist[:external_id]).to eq('123')
        expect(gist[:star_count]).to eq(50)
      end
    end

    context 'when gist does not exist' do
      it 'should create new gist' do
        gist = Gist.find_or_create('321')
        expect(gist[:external_id]).to eq('321')
        expect(gist[:star_count]).to be_nil
      end
    end
  end

  describe '.add_star' do
    it 'should increase star_count field by one' do
      gist = Gist.new(external_id: '123')
      gist.add_star
      expect(gist[:star_count]).to eq(1)
    end
  end

  describe '.remove_star' do
    it 'should decrease star_count field by one' do
      gist = Gist.new(external_id: '123', star_count: 3)
      gist.remove_star
      expect(gist[:star_count]).to eq(2)
    end
  end
end
