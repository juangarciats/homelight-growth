describe GistsController, type: :request do
  describe 'GET /gists' do
    before do
      @fake_github_gists = JSON.parse(file_fixture("github_public_gists.json").read)

      allow(Gist).to receive_message_chain(:where, :all).and_return([])
      allow(GithubApiGateway)
        .to receive(:get_public_gists)
        .and_return({
                      page: 1,
                      per_page: 10,
                      items: [@fake_github_gists[0]]
                    })

      get '/api/gists'
    end

    it 'should return paginated respoinse' do
      body = JSON.parse(response.body)
      expect(body['page']).to eq(1)
      expect(body['per_page']).to eq(10)
      expect(body['items'].length).to be > 0
    end

    it 'should transform each item of array' do
      body = JSON.parse(response.body)
      response_item = body['items'][0]
      expected_item = @fake_github_gists[0]
      expect(response_item['id']).to eq(expected_item['id'])
      expect(response_item['url']).to eq(expected_item['html_url'])
      expect(response_item['description']).to eq(expected_item['description'])
      expect(response_item['comments_count']).to eq(expected_item['comments'])
      expect(response_item['files_count']).to eq(expected_item['files'].length)
      expect(response_item['created_at']).to eq(expected_item['created_at'])
      expect(response_item['created_by']).to eq(expected_item['owner']['login'])
      expect(response_item['star_count']).to eq(0)
    end

    it 'should return a 200 status code' do
      expect(response.status).to eq(200)
    end

    context 'when gist exists on database' do
      let(:gist) { Gist.new(external_id: @fake_github_gists[0]['id'], star_count: 50) }

      before do
        allow(Gist).to receive_message_chain(:where, :all).and_return([gist])
        allow(GithubApiGateway)
          .to receive(:get_public_gists)
          .and_return({
                        page: 1,
                        per_page: 10,
                        items: [@fake_github_gists[0]]
                      })

        get '/api/gists'
      end

      it 'should return its star_count' do
        body = JSON.parse(response.body)
        response_item = body['items'][0]
        expect(response_item['star_count']).to eq(gist[:star_count])
      end
    end
  end

  describe 'PUT /gists/:id/star' do
    let(:gist) { Gist.new(external_id: 'abc') }

    before do
      allow(Gist).to receive(:find_or_create).and_return(gist)
      allow(gist).to receive(:add_star)
      put "/api/gists/#{gist[:external_id]}/star"
    end

    it 'should call method to add star' do
      expect(gist).to have_received(:add_star).once
    end

    it 'should return gist' do
      parsed_body = JSON.parse(response.body)
      expect(parsed_body['id']).to eq(gist[:id])
    end

    it 'should return a 201 status code' do
      expect(response.status).to eq(201)
    end
  end

  describe 'DELETE /gists/:id/star' do
    let(:gist) { Gist.new(external_id: 'abc') }

    before do
      allow(gist).to receive(:remove_star)
    end

    context 'when gist does not exist' do
      before do
        allow(Gist).to receive(:find_by).and_return(nil)
        delete "/api/gists/#{gist[:external_id]}/star"
      end

      it 'should not remove star' do
        expect(gist).to_not have_received(:remove_star)
      end

      it 'should return a 404 status code' do
        expect(response.status).to eq(404)
      end
    end

    context 'when gist exists' do
      before do
        allow(Gist).to receive(:find_by).and_return(gist)
        delete "/api/gists/#{gist[:external_id]}/star"
      end

      it 'should call method to remove star' do
        expect(gist).to have_received(:remove_star).once
      end

      it 'should return gist' do
        parsed_body = JSON.parse(response.body)
        expect(parsed_body['id']).to eq(gist[:id])
      end

      it 'should return a 200 status code' do
        expect(response.status).to eq(200)
      end
    end
  end
end
