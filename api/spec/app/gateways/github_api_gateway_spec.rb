describe GithubApiGateway do
  describe '.get_public_gists' do
    before do
      @fake_response = file_fixture("github_public_gists.json").read
      stub_request(:get, "https://api.github.com/gists/public?page=1&per_page=10")
        .to_return(body: @fake_response)
    end

    it 'should return paginated response' do
      result = GithubApiGateway.get_public_gists
      expect(result[:page]).to eq(1)
      expect(result[:per_page]).to eq(10)
      expect(result[:items].length).to eq(2)
      expect(result[:items][0]).to eq(JSON.parse(@fake_response)[0])
    end
  end
end
