class CreateGists < ActiveRecord::Migration[7.0]
  def change
    create_table :gists do |t|
      t.string :external_id
      t.integer :star_count

      t.timestamps
    end
  end
end
