# gists.

Small project to fetch and list public gists from Github.

This is a project built as a take-home assignment in order to join Growth team at Homelight. The requirements can be seen on [this Github repository](https://github.com/homelight/growth-interview).

![](docs/images/demo.gif)

## How to run

To run this project, you need to run both frontend and backend projects.

More instructions are available in the respective subfolder:

- [API](https://gitlab.com/juangarciats/homelight-growth/-/blob/main/api/README.md)
- [Web](https://gitlab.com/juangarciats/homelight-growth/-/blob/main/web/README.md)
